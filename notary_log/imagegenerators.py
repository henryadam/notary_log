from imagekit import ImageSpec, register
from imagekit.processors import ResizeToFill

class Thumbnail(ImageSpec):
    processors = [ResizeToFill(100, 50)]
    format = 'JPEG'
    options = {'quality': 60}


class Square(ImageSpec):
    processors = [ResizeToFill(200, 200)]
    format = 'JPEG'
    options = {'quality': 60}

class SquareSmall(ImageSpec):
    processors = [ResizeToFill(120, 120)]
    format = 'JPEG'
    options = {'quality': 60}

register.generator('notary_log:thumbnail', Thumbnail)
register.generator('notary_log:square', Square)
register.generator('notary_log:square_small', SquareSmall)
