# Create your views here.
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from extendedUserCreationForm import ExtendedUserCreationForm
from notary_log.models import Image, Passport, UserProfile, Journal, JournalEntry
from notary_log.randomNameGenerator import return_name
from django.contrib.auth.forms import AuthenticationForm
from django.core.files.storage import default_storage

# Create your views here.


def home(request):
    form = AuthenticationForm(request)
    journal_entries_to_display = []
    random_journal_entries_with_images = JournalEntry.objects.select_related().filter().order_by('?')[:15]

    for journal_entry in random_journal_entries_with_images:
        for image in journal_entry.images.all():
            if default_storage.exists(str(image.image)):
                journal_entries_to_display.append(journal_entry)


    my_data_dictionary = {
        "form": form,
        "journal_entries": journal_entries_to_display
    }
    return render_to_response('notary_log/home.html', my_data_dictionary, context_instance=RequestContext(request))


def register(request):
    if request.method == 'POST':
        form = ExtendedUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            #here after the user is created we will create the ancillary objects

            passport = Passport(title=return_name(), user=user)
            journal = Journal(title=return_name(), user=user)
            user_profile = UserProfile(user=user)

            passport.full_clean()
            passport.save()

            journal.full_clean()
            journal.save()

            user_profile.full_clean()
            user_profile.save()

        return HttpResponseRedirect("/accounts/login/")
    else:
        form = ExtendedUserCreationForm()
        return render(request, "registration/register.html", {
            'form': form,
        })
