# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'NotaryLogEntry'
        db.delete_table(u'notary_log_notarylogentry')

        # Adding model 'Passport'
        db.create_table(u'notary_log_passport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'notary_log', ['Passport'])

        # Adding model 'PassportStamp'
        db.create_table(u'notary_log_passportstamp', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('passport', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['notary_log.Passport'])),
        ))
        db.send_create_signal(u'notary_log', ['PassportStamp'])

        # Adding M2M table for field journal_entries on 'PassportStamp'
        m2m_table_name = db.shorten_name(u'notary_log_passportstamp_journal_entries')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('passportstamp', models.ForeignKey(orm[u'notary_log.passportstamp'], null=False)),
            ('journalentry', models.ForeignKey(orm[u'notary_log.journalentry'], null=False))
        ))
        db.create_unique(m2m_table_name, ['passportstamp_id', 'journalentry_id'])

        # Adding M2M table for field images on 'PassportStamp'
        m2m_table_name = db.shorten_name(u'notary_log_passportstamp_images')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('passportstamp', models.ForeignKey(orm[u'notary_log.passportstamp'], null=False)),
            ('image', models.ForeignKey(orm[u'notary_log.image'], null=False))
        ))
        db.create_unique(m2m_table_name, ['passportstamp_id', 'image_id'])

        # Adding model 'Image'
        db.create_table(u'notary_log_image', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('title', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'notary_log', ['Image'])

        # Adding M2M table for field passport on 'Image'
        m2m_table_name = db.shorten_name(u'notary_log_image_passport')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('image', models.ForeignKey(orm[u'notary_log.image'], null=False)),
            ('passport', models.ForeignKey(orm[u'notary_log.passport'], null=False))
        ))
        db.create_unique(m2m_table_name, ['image_id', 'passport_id'])

        # Adding M2M table for field journal on 'Image'
        m2m_table_name = db.shorten_name(u'notary_log_image_journal')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('image', models.ForeignKey(orm[u'notary_log.image'], null=False)),
            ('journal', models.ForeignKey(orm[u'notary_log.journal'], null=False))
        ))
        db.create_unique(m2m_table_name, ['image_id', 'journal_id'])

        # Adding model 'JournalEntry'
        db.create_table(u'notary_log_journalentry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('journal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['notary_log.Journal'])),
            ('journal_entry', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'notary_log', ['JournalEntry'])

        # Adding M2M table for field images on 'JournalEntry'
        m2m_table_name = db.shorten_name(u'notary_log_journalentry_images')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('journalentry', models.ForeignKey(orm[u'notary_log.journalentry'], null=False)),
            ('image', models.ForeignKey(orm[u'notary_log.image'], null=False))
        ))
        db.create_unique(m2m_table_name, ['journalentry_id', 'image_id'])

        # Adding model 'Journal'
        db.create_table(u'notary_log_journal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'notary_log', ['Journal'])

        # Deleting field 'UserProfile.city'
        db.delete_column(u'notary_log_userprofile', 'city')

        # Deleting field 'UserProfile.billing'
        db.delete_column(u'notary_log_userprofile', 'billing')

        # Deleting field 'UserProfile.address1'
        db.delete_column(u'notary_log_userprofile', 'address1')

        # Deleting field 'UserProfile.address2'
        db.delete_column(u'notary_log_userprofile', 'address2')

        # Deleting field 'UserProfile.extra'
        db.delete_column(u'notary_log_userprofile', 'extra')

        # Deleting field 'UserProfile.first_day_of_week'
        db.delete_column(u'notary_log_userprofile', 'first_day_of_week')

        # Deleting field 'UserProfile.date_of_birth'
        db.delete_column(u'notary_log_userprofile', 'date_of_birth')

        # Deleting field 'UserProfile.state'
        db.delete_column(u'notary_log_userprofile', 'state')

        # Deleting field 'UserProfile.gender'
        db.delete_column(u'notary_log_userprofile', 'gender')

        # Deleting field 'UserProfile.zip_code'
        db.delete_column(u'notary_log_userprofile', 'zip_code')

        # Deleting field 'UserProfile.company'
        db.delete_column(u'notary_log_userprofile', 'company')

        # Deleting field 'UserProfile.ein'
        db.delete_column(u'notary_log_userprofile', 'ein')


    def backwards(self, orm):
        # Adding model 'NotaryLogEntry'
        db.create_table(u'notary_log_notarylogentry', (
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'notary_log', ['NotaryLogEntry'])

        # Deleting model 'Passport'
        db.delete_table(u'notary_log_passport')

        # Deleting model 'PassportStamp'
        db.delete_table(u'notary_log_passportstamp')

        # Removing M2M table for field journal_entries on 'PassportStamp'
        db.delete_table(db.shorten_name(u'notary_log_passportstamp_journal_entries'))

        # Removing M2M table for field images on 'PassportStamp'
        db.delete_table(db.shorten_name(u'notary_log_passportstamp_images'))

        # Deleting model 'Image'
        db.delete_table(u'notary_log_image')

        # Removing M2M table for field passport on 'Image'
        db.delete_table(db.shorten_name(u'notary_log_image_passport'))

        # Removing M2M table for field journal on 'Image'
        db.delete_table(db.shorten_name(u'notary_log_image_journal'))

        # Deleting model 'JournalEntry'
        db.delete_table(u'notary_log_journalentry')

        # Removing M2M table for field images on 'JournalEntry'
        db.delete_table(db.shorten_name(u'notary_log_journalentry_images'))

        # Deleting model 'Journal'
        db.delete_table(u'notary_log_journal')

        # Adding field 'UserProfile.city'
        db.add_column(u'notary_log_userprofile', 'city',
                      self.gf('django.db.models.fields.CharField')(default='orlando', max_length=150),
                      keep_default=False)

        # Adding field 'UserProfile.billing'
        db.add_column(u'notary_log_userprofile', 'billing',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=150, blank=True),
                      keep_default=False)

        # Adding field 'UserProfile.address1'
        db.add_column(u'notary_log_userprofile', 'address1',
                      self.gf('django.db.models.fields.CharField')(default='1234 fun street', max_length=150),
                      keep_default=False)

        # Adding field 'UserProfile.address2'
        db.add_column(u'notary_log_userprofile', 'address2',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=150, blank=True),
                      keep_default=False)

        # Adding field 'UserProfile.extra'
        db.add_column(u'notary_log_userprofile', 'extra',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'UserProfile.first_day_of_week'
        db.add_column(u'notary_log_userprofile', 'first_day_of_week',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=10, blank=True),
                      keep_default=False)

        # Adding field 'UserProfile.date_of_birth'
        db.add_column(u'notary_log_userprofile', 'date_of_birth',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 3, 22, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'UserProfile.state'
        db.add_column(u'notary_log_userprofile', 'state',
                      self.gf('django.db.models.fields.CharField')(default='FL', max_length=2),
                      keep_default=False)

        # Adding field 'UserProfile.gender'
        db.add_column(u'notary_log_userprofile', 'gender',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=1, blank=True),
                      keep_default=False)

        # Adding field 'UserProfile.zip_code'
        db.add_column(u'notary_log_userprofile', 'zip_code',
                      self.gf('django.db.models.fields.CharField')(default='32345', max_length=150),
                      keep_default=False)

        # Adding field 'UserProfile.company'
        db.add_column(u'notary_log_userprofile', 'company',
                      self.gf('django.db.models.fields.CharField')(default='Some Company', max_length=50),
                      keep_default=False)

        # Adding field 'UserProfile.ein'
        db.add_column(u'notary_log_userprofile', 'ein',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=9, blank=True),
                      keep_default=False)


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'notary_log.image': {
            'Meta': {'object_name': 'Image'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'journal': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['notary_log.Journal']", 'symmetrical': 'False', 'blank': 'True'}),
            'passport': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['notary_log.Passport']", 'symmetrical': 'False', 'blank': 'True'}),
            'title': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'notary_log.journal': {
            'Meta': {'object_name': 'Journal'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'notary_log.journalentry': {
            'Meta': {'object_name': 'JournalEntry'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['notary_log.Image']", 'symmetrical': 'False'}),
            'journal': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notary_log.Journal']"}),
            'journal_entry': ('django.db.models.fields.TextField', [], {}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'notary_log.passport': {
            'Meta': {'object_name': 'Passport'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'notary_log.passportstamp': {
            'Meta': {'object_name': 'PassportStamp'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['notary_log.Image']", 'symmetrical': 'False', 'blank': 'True'}),
            'journal_entries': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['notary_log.JournalEntry']", 'symmetrical': 'False', 'blank': 'True'}),
            'passport': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notary_log.Passport']"}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'notary_log.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'secondary_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        }
    }

    complete_apps = ['notary_log']