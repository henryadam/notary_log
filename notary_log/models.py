from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
import simpleimages.transforms
import simpleimages.trackers
import simpleimages.trackers
from uuidfield import UUIDField
from notary_log.image_helpers import unique_filename
# Create your models here.

class CommonInfo(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class UserProfile(CommonInfo):
    user = models.ForeignKey(User, unique=True)
    secondary_email = models.EmailField(blank=True)

    def __str__(self):
        return "USER PROFILE: %s | %s %s" % (self.user.username, self.user.first_name, self.user.last_name)


class Passport(CommonInfo):
    """
        Describes a users passport, like in real life one use can only have one passport

    """
    user = models.ForeignKey(User, unique=True)
    title = models.CharField(blank=False, max_length=255)
    description = models.TextField(blank=True)

    def __str__(self):
        return "Passport: %s | %s %s" % (self.user.username, self.user.first_name, self.user.last_name)


class Journal(CommonInfo):
    user = models.ForeignKey(User, unique=True)
    title = models.CharField(blank=False, max_length=255)
    description = models.TextField(blank=True)

    def __str__(self):
        return "Journal: %s | %s %s" % (self.user.username, self.user.first_name, self.user.last_name)


class Image(CommonInfo):
    """
        This is ne of the root models of the application, an image can belong to a multitude of entities
        with the many to many
    """
    passport = models.ManyToManyField(Passport, blank=True)
    journal = models.ManyToManyField(Journal, blank=True)
    image = models.ImageField(upload_to=unique_filename('images'))
    title = models.TextField(blank=True)

    thumbnail_image = models.ImageField(
        blank=True,
        null=True,
        editable=False,
        upload_to=unique_filename('images/thumbnails/')
    )
    large_image = models.ImageField(
        blank=True,
        null=True,
        editable=False,
        upload_to=unique_filename('images/large/')
    )

    transformed_fields = {
        'image': {
            'thumbnail_image': simpleimages.transforms.Scale(width=50),
            'large_image': simpleimages.transforms.Scale(width=200),
        }
    }



    def __str__(self):
        return "Image: %s" % (
        self.title)


class JournalEntry(CommonInfo):
    """
        a journal entry can have many images attached but belong to ony one journal
    """
    journal = models.ForeignKey(Journal)
    images = models.ManyToManyField(Image, blank=True)
    journal_entry = models.TextField(blank=False)

    def __str__(self):
        return "Journal Entry: %s | %s %s" % (
        self.journal.user.username, self.journal.user.first_name, self.journal.user.last_name)


class PassportStamp(CommonInfo):
    passport = models.ForeignKey(Passport)
    journal_entries = models.ManyToManyField(JournalEntry, blank=True)
    images = models.ManyToManyField(Image, blank=True)

    def __str__(self):
        return "Passport Stamp: %s | %s %s" % (
        self.passport.user.username, self.passport.user.first_name, self.passport.user.last_name)


simpleimages.trackers.track_model(Image)

admin.site.register(UserProfile)
admin.site.register(Passport)
admin.site.register(Journal)
admin.site.register(Image)
admin.site.register(JournalEntry)
admin.site.register(PassportStamp)



