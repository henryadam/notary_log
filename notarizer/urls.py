from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    # Examples:

        url(r'^$', 'notarizer.views.home', name='notarizer'),
        url(r'^add-journal-entry', 'notarizer.views.create_journal_entry', name='create_journal_entry'),
        url(r'^journal-entry', 'notarizer.views.list_journal_entries', name='list_journal_entries'),
        # url(r'^lists', 'secure.views.lists', name='secure_lists'),
        # url(r'^utilities', 'secure.views.utilities', name='secure_utilities'),
        # url(r'^reports', 'secure.views.reports', name='secure_reports'),
        # url(r'^account', 'secure.views.account', name='secure_accounts'),
        # url(r'^search', 'secure.views.search', name='secure_search'),
        # url(r'^visualize-list/', 'secure.views.list_items_as_layers', name='list_items_as_layers'),
        # url(r'^list/', include('list.urls')),
        # url(r'^list-item/', include('listitem.urls')),
        # url(r'^clients/', include('clients.urls'))
)

urlpatterns += staticfiles_urlpatterns()