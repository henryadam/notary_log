from django.core import serializers
from django.conf import settings
from django.forms.models import inlineformset_factory
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.db.models import Q
from django.db.models import Sum
from django.forms.models import model_to_dict
import simplejson as json
from notary_log.randomNameGenerator import return_name
from notary_log.models import Image, JournalEntry, Journal, Passport
import locale
from django.core.files.storage import default_storage
from django.shortcuts import redirect
locale.setlocale( locale.LC_ALL, 'en_US.UTF-8' )
# #for case_type in request.POST.getlist('case_type'):
#             case_type_for_prospect = case_types.get(pk=case_type)
#             prospect.case_type.add(case_type_for_prospect)

@login_required
def home(request):
    """
        This is the home page for the notarizer, we will display a cool form to add journal entries
    """
    #get my journal
    journal = Journal.objects.select_related().get(user=request.user)
    journal_entries_to_display = []
    #add the posted data will be a journal entry
    if request.method == "POST":
       image = Image()
       image.image = request.FILES['journal_entry_image']
       image.title = return_name()
       image.save()

       journal_entry = JournalEntry(journal=journal)
       journal_entry.journal_entry = request.POST["journal_entry"]

       journal_entry.save()
       journal_entry.images.add(image)


    journal_entries = journal.journalentry_set.all().order_by('-created')

    for journal_entry in journal_entries:
        for image in journal_entry.images.all():
            if default_storage.exists(str(image.image)):
                journal_entries_to_display.append(journal_entry)


    #get my passport
    passport = Passport.objects.select_related().get(user=request.user)
    passport_stamps = passport.passportstamp_set.all()




    data_dictionary = {
        "journal_title": journal.title,
        "journal_entries":journal_entries_to_display

    }
    return render_to_response(
        'notarizer/home.html',
        data_dictionary,
        context_instance=RequestContext(request))


@login_required
def create_journal_entry(request):
    """
        This is the home page for the notarizer, we will display a cool form to add journal entries
    """
    #get my journal
    journal = Journal.objects.get(user=request.user)

    #add the posted data will be a journal entry
    if request.method == "POST":
       image = Image()
       image.image = request.FILES['journal_entry_image']
       image.title = return_name()
       image.save()

       journal_entry = JournalEntry(journal=journal)
       journal_entry.journal_entry = request.POST["journal_entry"]

       journal_entry.save()
       journal_entry.images.add(image)
    return redirect('list_journal_entries')

@login_required
def list_journal_entries(request):
    """
        This is the home page for the notarizer, we will display a cool form to add journal entries
    """
    #get my journal
    journal = Journal.objects.select_related().get(user=request.user)
    journal_entries_to_display = []
    #add the posted data will be a journal entry
    if request.method == "POST":
       image = Image()
       image.image = request.FILES['journal_entry_image']
       image.title = return_name()
       image.save()

       journal_entry = JournalEntry(journal=journal)
       journal_entry.journal_entry = request.POST["journal_entry"]

       journal_entry.save()
       journal_entry.images.add(image)


    journal_entries = journal.journalentry_set.all().order_by('-created')

    for journal_entry in journal_entries:
        for image in journal_entry.images.all():
            if default_storage.exists(str(image.image)):
                journal_entries_to_display.append(journal_entry)


    #get my passport
    passport = Passport.objects.select_related().get(user=request.user)
    passport_stamps = passport.passportstamp_set.all()




    data_dictionary = {
        "journal_title": journal.title,
        "journal_entries":journal_entries_to_display

    }
    return render_to_response(
        'notarizer/journal_entry_personal_feed.html',
        data_dictionary,
        context_instance=RequestContext(request))
