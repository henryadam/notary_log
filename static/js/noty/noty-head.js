function notify(bare_noty) {
    // the idea here is that we have a central location now for notifications in case we want to
    // multi-cast these messages to other users or servers
    noty(bare_noty);
}